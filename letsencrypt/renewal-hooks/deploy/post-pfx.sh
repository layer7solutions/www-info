#!/bin/sh

set -e

REQ_LINEAGE="/etc/letsencrypt/live/layer7.solutions"

echo "Running post-pfx.sh"
echo "  RENEWED_LINEAGE=$RENEWED_LINEAGE"
echo "  RENEWED_DOMAINS=$RENEWED_DOMAINS"
echo "  REQUIRED_LINEAGE=$REQ_LINEAGE"

if [ "$RENEWED_LINEAGE" != "$REQ_LINEAGE" ]; then
    echo "  Lineage does not match, exiting"
    exit 0
fi

if [ ! -d "/opt/skynet/bamboo_home" ]; then
    echo "  Target cert directory does not exist, exiting"
    exit 0
fi

echo "  Creating pfx bundle"
openssl pkcs12 -export -out /opt/skynet/bamboo_home/bundle.pfx -inkey $REQ_LINEAGE/privkey.pem -in $REQ_LINEAGE/cert.pem -certfile $REQ_LINEAGE/chain.pem -password pass:PZYpVjCntCEhLkhwylZh

chown server "/opt/skynet/bamboo_home/bundle.pfx"
chmod 400 "/opt/skynet/bamboo_home/bundle.pfx"

echo "  Restarting Bamboo"
systemctl restart bamboo >/dev/null
