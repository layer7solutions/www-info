#!/bin/sh

set -e

REQ_LINEAGE="/etc/letsencrypt/live/layer7.solutions"

echo "Running post-chronograf.sh"
echo "  RENEWED_LINEAGE=$RENEWED_LINEAGE"
echo "  RENEWED_DOMAINS=$RENEWED_DOMAINS"
echo "  REQUIRED_LINEAGE=$REQ_LINEAGE"

if [ "$RENEWED_LINEAGE" != "$REQ_LINEAGE" ]; then
    echo "  Lineage does not match, exiting"
    exit 0
fi

daemon_cert_root=/etc/chronograf/certs

if [ ! -d "$daemon_cert_root" ]; then
    echo "  Target cert directory does not exist, exiting"
    exit 0
fi

for domain in $RENEWED_DOMAINS; do
        case $domain in
        layer7.solutions)
                # Make sure the certificate and private key files are
                # never world readable, even just for an instant while
                # we're copying them into daemon_cert_root.
                umask 077

		echo "  Copying files"
                cp "$RENEWED_LINEAGE/fullchain.pem" "$daemon_cert_root/$domain.cert"
                cp "$RENEWED_LINEAGE/privkey.pem" "$daemon_cert_root/$domain.key"

                # Apply the proper file ownership and permissions for
                # the daemon to read its certificate and key.
                chown chronograf "$daemon_cert_root/$domain.cert" \
                        "$daemon_cert_root/$domain.key"
                chmod 400 "$daemon_cert_root/$domain.cert" \
                        "$daemon_cert_root/$domain.key"

		echo "  Restarting chronograf"
                service chronograf restart >/dev/null
                ;;
        esac
done
