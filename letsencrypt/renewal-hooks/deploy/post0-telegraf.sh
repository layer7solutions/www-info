#!/bin/sh

set -e

REQ_LINEAGE="/etc/letsencrypt/live/layer7.solutions"

echo "Running post-telegraf.sh"
echo "  RENEWED_LINEAGE=$RENEWED_LINEAGE"
echo "  RENEWED_DOMAINS=$RENEWED_DOMAINS"
echo "  REQUIRED_LINEAGE=$REQ_LINEAGE"

if [ "$RENEWED_LINEAGE" != "$REQ_LINEAGE" ]; then
    echo "  Lineage does not match, exiting"
    exit
fi

daemon_cert_root=/etc/telegraf/certs

if [ ! -d "$daemon_cert_root" ]; then
    echo "  Target cert directory does not exist, exiting"
    exit 0
fi

for domain in $RENEWED_DOMAINS; do
        case $domain in
        layer7.solutions)
                # Make sure the certificate and private key files are
                # never world readable, even just for an instant while
                # we're copying them into daemon_cert_root.
                umask 077

		echo "  Copying files"
                cat "$RENEWED_LINEAGE/fullchain.pem" "$RENEWED_LINEAGE/privkey.pem" > "$daemon_cert_root/$domain.pem"
		cp "$RENEWED_LINEAGE/fullchain.pem" "$daemon_cert_root/$domain.cert"
		cp "$RENEWED_LINEAGE/privkey.pem" "$daemon_cert_root/$domain.key"

                # Apply the proper file ownership and permissions for
                # the daemon to read its certificate and key.
                chown telegraf "$daemon_cert_root/$domain.pem" "$daemon_cert_root/$domain.cert" "$daemon_cert_root/$domain.key"
                chmod 400 "$daemon_cert_root/$domain.pem" "$daemon_cert_root/$domain.cert" "$daemon_cert_root/$domain.key"

		echo "  Restarting telegraf"
                service telegraf restart >/dev/null
                ;;
        esac
done
