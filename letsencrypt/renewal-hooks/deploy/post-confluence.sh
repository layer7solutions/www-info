#!/bin/sh

set -e

REQ_LINEAGE="/etc/letsencrypt/live/layer7.solutions"

echo "Running post-confluence.sh"
echo "  RENEWED_LINEAGE=$RENEWED_LINEAGE"
echo "  RENEWED_DOMAINS=$RENEWED_DOMAINS"
echo "  REQUIRED_LINEAGE=$REQ_LINEAGE"

if [ "$RENEWED_LINEAGE" != "$REQ_LINEAGE" ]; then
    echo "  Lineage does not match, exiting"
    exit 0
fi

daemon_cert_root=/opt/atlassian/confluence/conf

if [ ! -d "$daemon_cert_root" ]; then
    echo "  Target cert directory does not exist, exiting"
    exit 0
fi

for domain in $RENEWED_DOMAINS; do
        case $domain in
        layer7.solutions)
                # Make sure the certificate and private key files are
                # never world readable, even just for an instant while
                # we're copying them into daemon_cert_root.
                umask 077

		echo "  Copying files"
		cp "$RENEWED_LINEAGE/cert.pem" "$daemon_cert_root/cert.pem"
                cp "$RENEWED_LINEAGE/chain.pem" "$daemon_cert_root/chain.pem"
                cp "$RENEWED_LINEAGE/privkey.pem" "$daemon_cert_root/privkey.pem"

                # Apply the proper file ownership and permissions for
                # the daemon to read its certificate and key.
                chown confluence "$daemon_cert_root/cert.pem" \
                        "$daemon_cert_root/chain.pem" \
			"$daemon_cert_Root/privkey.pem"
                chmod 400 "$daemon_cert_root/cert.pem" \
                        "$daemon_cert_root/chain.pem" \
			"$daemon_cert_root/privkey.pem"

		echo "  Restarting confluence"
                /etc/init.d/confluence stop >/dev/null
		/etc/init.d/confluence start >/dev/null
                ;;
        esac
done
