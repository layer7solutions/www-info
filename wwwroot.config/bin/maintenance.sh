#!/bin/bash

# All of our VirtualHosts that support maintenance mode include the respective
# '/var/www/.config/enabled/{maintenance|maintenancedev}.conf' file using the
# 'Include' directive before the VirtualHost's primary ".htinclude" file.
#
# This script changes the respective enabled/ symlink to "available/maintenance.conf"
# for maintenance mode enabled or "available/maintenance.conf.disabled" for disabled.

Mode=''

show_usage ()
{
    echo "Usage: sh $0 {production|dev|all-hosts} {enable|disable}"
    exit 1
}

maintenance_prod ()
{
    ln -sf /var/www/.config/available/maintenance.conf$Mode /var/www/.config/enabled/maintenance.conf
}

maintenance_dev ()
{
    ln -sf /var/www/.config/available/maintenance.conf$Mode /var/www/.config/enabled/maintenancedev.conf
}

case "$2" in
    enable )
        Mode=''
        ;;
    disable )
        Mode='.disabled'
        ;;
    * )
        show_usage
        ;;
esac

case "$1" in
    all-hosts )
        maintenance_prod
        maintenance_dev
        ;;
    production )
        maintenance_prod
        ;;
    dev )
        maintenance_dev
        ;;
    * )
        show_usage
        ;;
esac

service apache2 reload
