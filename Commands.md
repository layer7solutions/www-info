# Useful Commands to Know

## Restarting Apache2 service

  - **Restart Apache2:** `sudo service apache2 restart`
  - **Stop Apache2:** `sudo service apache2 stop`
  - **Start Apache2:** `sudo service apache2 start`

> **Alternative syntax:**
>
> These are all way of restarting Apache2. These
> four commands all have the same effect.
>
> ```
> sudo service apache2 restart
> sudo apachectl restart
> sudo apache2ctl restart
> sudo systemctl restart apache2
> ```
>
> Note: apachectl is an alias of apache2ctl

## Restarting PHP service

Note: restarting Apache2 does not restart PHP and vice-versa.

  - **Restart PHP:** `sudo systemctl restart php{ver}-fpm`
  - **Stop PHP:** `sudo systemctl stop php{ver}-fpm`
  - **Start PHP:** `sudo systemctl start php{ver}-fpm`

Replace ver with the current version of PHP being used.
e.g. `sudo systemctl restart php7.3-fpm`

## Enable/Disable Maintenance Mode

**Usage:** `sh /var/www/.config/bin/maintenance.sh {production|dev|all-hosts} {enable|disable}`

 - production: only layer7.solutions and destinyreddit.com
 - dev: only beta.layer7.solutions and staging.destinyreddit.com
 - all-hosts: both production and dev

When maintenance mode is enabled on a site, all requests to that site (except when requesting `/favicon.ico`) are pointed to this static file: `/var/www/static.layer7.solutions/public/maintenance.html`

## How to provision SSL certificates

### For a new subdomain

You don't. Our certificates should all be wildcard certificates, so subdomains should be automagically handled.

### For a new domain

Run this command:

```sudo certbot certonly --agree-tos -m webmaster@layer7.solutions --cert-name {DOMAIN} --dns-cloudflare --dns-cloudflare-credentials /root/.secrets/cloudflare.ini -d {DOMAIN},*.{DOMAIN} --preferred-challenges dns-01```

Obviously replacing `{DOMAIN}` with the domain you're creating a wildcard certificate for. For example:


```sudo certbot certonly --agree-tos -m webmaster@layer7.solutions --cert-name example.com --dns-cloudflare --dns-cloudflare-credentials /root/.secrets/cloudflare.ini -d example.com,*.example.com --preferred-challenges dns-01```

And that's it! Renewal is automatically handled, there's nothing else you need to run.

Your SSLCertificateFile is at `/etc/letsencrypt/live/{DOMAIN}/fullchain.pem` and your SSLCertificateKeyFile is at `/etc/letsencrypt/live/layer7.solutions/privkey.pem`.

Check out [the apache2/sites-templates folder](apache2/sites-templates) for how to configure a new Apache2 site. Replace the {SUBDOMAIN} and {DOMAIN} tokens as needed and place the site configuration file in `/etc/apache2/sites-available` and run `sudo a2ensite {YourSiteName}.conf` then run `sudo service apache2 reload` and that should get the site running.